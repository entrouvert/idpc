/*
 *  idpc - IDP as a C CGI program
 *  Copyright (C) 2004-2005 Entr'ouvert
 * 
 *  Authors: See AUTHORS file in top-level directory.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "idpc.h"
#include <libxml/uri.h>

int error_page(char *msg)
{
	fprintf(stderr, "%s\n", msg);
	printf("Status: 500 Server Error\n");
	printf("Content-type: text/plain\n\n");
	printf("Error; %s\n", msg);
	return 1;
}

int handle_args(int argc, char *argv[])
{
	int i;
	int rc = 0;

	for (i=1; i<argc; i++) {
		if (strcmp(argv[i], "--help") == 0) {
			printf( "This is IdPC; it is meant to be used as a CGI"
				"\n\n"
				"Config file should be installed as:\n"
				"  %s\n", SYSCONFDIR "config.xml");
			rc = 1;
			continue;
		}
	}

	return rc;
}

/**
 * urlencoded_to_strings
 * @str: url encoded query string
 *
 * Description:
 * Unquote and divide fields of a query string
 *
 * Returns: NULL-terminated array of pointers to individual fields
 **/
char** urlencoded_to_strings(char *str)
{
	int i, n=1;
	char *st, *st2;
	char **result;

	st = str;
	while (*st != '\0') {
		if (*st == '&') n++;
		st++;
	}

	result = malloc(sizeof(char*)*(n+1));
	result[n] = NULL;

	st = str;
	for (i=0; i<n; i++) {
		st2 = strchr(st, '&');
		st2 = st2 ? st2 : st+strlen(st);
		result[i] = malloc(st2-st);
		xmlURIUnescapeString(st, st2-st, result[i]);
		st = st2 + 1;
	}
	return result;
}

int set_profile_from_dumps(LassoProfile *profile,
			   char *identity_dump, char *session_dump)
{
	int rc;

	if (identity_dump && identity_dump[0]) {
		rc = lasso_profile_set_identity_from_dump(profile, identity_dump);
		if (rc) {
			fprintf(stderr, "set identity failed\n");
			return 1;
		}
	}

	if (session_dump && session_dump[0]) {
		rc = lasso_profile_set_session_from_dump(profile, session_dump);
		if (rc) {
			fprintf(stderr, "set session failed\n");
			return 1;
		}
	}

	return 0;
}

/**
 * set_profile_auto
 * @profile
 *
 * Description:
 * restore profile from database dumps; based on the name identifier
 * already set in the profile.
 *
 * Returns: 0 on success, 1 on failure
 */
int set_profile_auto(LassoProfile *profile)
{
	int rc;
	char *user_id;
	char *identity_dump, *session_dump;

	rc = db_get_user_id(profile->nameIdentifier->content, &user_id);
	if (rc) {
		fprintf(stderr, "db_get_user_id failed\n");
		return 1;
	}

	rc = db_get_dumps(user_id, &identity_dump, &session_dump);
	free(user_id);
	if (rc) {
		fprintf(stderr, "Error getting dumps from db\n");
		return 1;
	}

	rc = set_profile_from_dumps(profile, identity_dump, session_dump);
	free(identity_dump);
	free(session_dump);
	return rc;
}


/**
 * save_profile_dumps
 * @profile
 *
 * Description:
 * stores identity and session dumps of profile in the database
 *
 * Returns: 0 on success, 1 on failure
 */
int save_profile_dumps(LassoProfile *profile)
{
	int rc;
	char *dump;
	char *user_id;

	rc = db_get_user_id(profile->nameIdentifier->content, &user_id);
	if (rc) {
		fprintf(stderr, "db_get_user_id failed\n");
		return 1;
	}
	fprintf(stderr, "INFO: Save session & identity for user: %s\n", user_id);

	if (lasso_profile_is_identity_dirty(profile)) {
		LassoIdentity *identity;
		identity = lasso_profile_get_identity(profile);
		dump = identity ? lasso_identity_dump(identity) : strdup("");
		rc = db_save_identity(user_id, dump);
		free(dump);
		if (rc) {
			fprintf(stderr, "db_save_identity failed: error %d\n", rc);
			free(user_id);
			return 1;
		}
	}

	if (lasso_profile_is_session_dirty(profile)) {
		LassoSession *session;
		session = lasso_profile_get_session(profile);
		dump = session ? lasso_session_dump(session) : strdup("");
		rc = db_save_session(user_id, dump);
		free(dump);
		if (rc) {
			fprintf(stderr, "db_save_session failed: error %d\n", rc);
			free(user_id);
			return 2;
		}
	}

	free(user_id);
	return 0;
}

char* strtime(time_t tim)
{
	struct tm *tm;
	char *s;
	
	tm = localtime(&tim);
	s = malloc(22);

	strftime(s, 22, "%Y-%m-%dT%H:%M:%SZ", tm);

	return s;
}

