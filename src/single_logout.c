/*
 *  idpc - IDP as a C CGI program
 *  Copyright (C) 2004-2005 Entr'ouvert
 * 
 *  Authors: See AUTHORS file in top-level directory.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "idpc.h"

int soap_loop(LassoLogout *logout);
int logout_http(LassoLogout *logout);
int logout_init(LassoLogout *logout);
int logout_req();

/* the Single Log-Out service URL handles several cases:
 *  1a. initiated by IdP, HTTP-Redirect (fig12, page47)
 *  1b. initiated by IdP, HTTP-GET (fig13, page50)
 *  1c. initiated by IdP, SOAP (fig14, page52)
 *  2a. initiated by SP, HTTP-Redirect (fig15, page53)
 *  [!2b. initiated by SP, SOAP is handled in soapEndpoint!]
 */

int soap_loop(LassoLogout *logout)
{
	char *other_sp;
	int rc;
	char *soap_answer;

	other_sp = lasso_logout_get_next_providerID(logout);
	while (other_sp) {
		fprintf(stderr, "Other SP: %s\n", other_sp);
		rc = lasso_logout_init_request(logout, other_sp, LASSO_HTTP_METHOD_ANY);
		if (rc) {
			fprintf(stderr, "init_request failed\n");
			return 1;
		}
		rc = lasso_logout_build_request_msg(logout);
		if (rc) {
			fprintf(stderr, "build_request failed\n");
			return 1;
		}
		if (LASSO_PROFILE(logout)->msg_body) {
			soap_answer = soap_request(
					LASSO_PROFILE(logout)->msg_url,
					LASSO_PROFILE(logout)->msg_body, NULL);
			if (soap_answer == NULL) {
				fprintf(stderr, "soap_request failed\n");
				return 1;
			}

			rc = lasso_logout_process_response_msg(logout, soap_answer);
			if (rc) {
				free(soap_answer);
				fprintf(stderr, "logout_process_response_msg failed\n");
				return 1;
			}
			free(soap_answer);
		} else {
			/* LASSO_PROFILE(logout)->msg_body == NULL
			 * that means it didn't support SOAP; ignore it for
			 * now.
			 */
		}
			
		other_sp = lasso_logout_get_next_providerID(logout);
	}

	lasso_logout_reset_providerID_index(logout);
	other_sp = lasso_logout_get_next_providerID(logout);
	if (other_sp) {
		/* remaining service provider; redirect */
		rc = lasso_logout_init_request(logout, other_sp,
				LASSO_HTTP_METHOD_REDIRECT);
		if (rc) {
			fprintf(stderr, "lasso_logout_init_request failed\n");
			return 0;
		}
		rc = lasso_logout_build_request_msg(logout);
		if (rc) {
			fprintf(stderr, "lasso_logout_build_request_msg\n");
			return 0;
		}

		printf("Location: %s\n\nRedirected",
				LASSO_PROFILE(logout)->msg_url);
	} else {
		/* we're done */
		rc = lasso_logout_build_request_msg(logout);
		if (rc == LASSO_PROFILE_ERROR_MISSING_REQUEST) {
			/* initiated from IdP */
			printf("Content-type: text/plain\n\nLogged out");
			return 0;
		}
		if (rc) {
			printf("Location: %s\n\nRedirected",
					LASSO_PROFILE(logout)->msg_url);
			return 0;
		}
	}

	return 0;
}


int logout_http(LassoLogout *logout)
{
	/* SP-initiated; HTTP-Redirect-Based Profile */
	int rc;

	rc = set_profile_auto(LASSO_PROFILE(logout));
	if (rc) {
		return error_page("set_profile_auto failed");
	}

	/* alternate soap/redirect profile; could use HTTP-GET here */
	return soap_loop(logout);
}


int logout_init(LassoLogout *logout)
{
	/* IdP-initiated */
	int rc;
	struct authentication *auth;
	char *user_id;
	char *identity_dump, *session_dump;

	auth = get_authentication(
			get_config_string("//idpc:authenticationMethod"));
	if (auth == NULL) {
		return error_page("Wrong authentication");
	}

	user_id = auth->auth_function();

	if (user_id == NULL) {
		/* anyway */
		return error_page("Error authenticating");
	}

	/* retrieve identity_dump and session_dump */
	rc = db_get_dumps(user_id, &identity_dump, &session_dump);
	if (rc) {
		return error_page("Error getting dumps from db");
	}
	rc = set_profile_from_dumps(LASSO_PROFILE(logout),
			identity_dump, session_dump);
	free(identity_dump);
	free(session_dump);
	if (rc) {
		return error_page("Error setting dumps");
	}

	return soap_loop(logout);
}


int logout_req()
{
	LassoLogout *logout;
	LassoServer *server;
	int rc;

	server = get_config_server();
	if (server == NULL) {
		lasso_server_destroy(server);
		return error_page("Failed to get server configuration");
	}

	logout = lasso_logout_new(server);
	if (logout == NULL) {
		lasso_server_destroy(server);
		return error_page("lasso_logout_new failed");
	}

	rc = lasso_logout_process_request_msg(logout, getenv("QUERY_STRING"));
	if (rc == LASSO_PROFILE_ERROR_INVALID_QUERY) {
		/* unknown query string; initiate logout now */
		rc = logout_init(logout);
	} else {
		if (rc) {
			fprintf(stderr,
				"lasso_logout_process_request_msg failed\n");
		} else {
			/* this is http-redirect profile; coming from SP */
			rc = logout_http(logout);
		}
	}

	lasso_logout_destroy(logout);
	lasso_server_destroy(server);

	return rc;
}


int main(int argc, char *argv[])
{
	int rc;

	if (argc > 1 && handle_args(argc, argv) ) {
		return 0;
	}

	rc = init_config();
	if (rc != 0) {
		return error_page("Failed to init configuration");
	}
	lasso_init();
	rc = db_init();
	if (rc != 0) {
		error_page("Failed to init database access");
		goto shutdown;
	}
	rc = logout_req();
shutdown:
	db_finish();
	lasso_shutdown();
	return rc;
}
