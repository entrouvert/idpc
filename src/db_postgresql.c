/*
 *  idpc - IDP as a C CGI program
 *  Copyright (C) 2004-2005 Entr'ouvert
 * 
 *  Authors: See AUTHORS file in top-level directory.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "idpc.h"

#ifdef USE_POSTGRESQL

#include <libpq-fe.h>

static PGconn *sqlconn;

int db_init()
{
	sqlconn = PQsetdbLogin(
			get_config_string("//idpc:dbhost"),
			get_config_string("//idpc:dbport"),
			get_config_string("//idpc:dboptions"),
			get_config_string("//idpc:dbtty"),
			get_config_string("//idpc:dbname"),
			get_config_string("//idpc:dblogin"),
			get_config_string("//idpc:dbpassword"));
	if (PQstatus(sqlconn) == CONNECTION_BAD) {
		PQfinish(sqlconn);
		sqlconn = NULL;
		return 1;
	}
	return 0;
}

int db_get_dumps(char *user_id, char **identity_dump, char **session_dump)
{
	PGresult *res;
	const char *params[1];

	params[0] = user_id;

	res = PQexecParams(sqlconn,
			"SELECT identity_dump, session_dump FROM users "
			"WHERE user_id = $1",
			1, NULL, params, NULL, NULL, 1);
	if (PQresultStatus(res) != PGRES_TUPLES_OK || PQntuples(res) == 0) {
		/* error or no tuple for this nameidentifier */
		PQclear(res);
		*identity_dump = NULL;
		*session_dump = NULL;
		return 1;
	}
	*identity_dump = strdup(PQgetvalue(res, 0, 0));
	*session_dump = strdup(PQgetvalue(res, 0, 1));
	PQclear(res);
	return 0;
}

static int ensure_user(char *user_id)
{
	PGresult *res;
	const char *params[1];

	params[0] = user_id;

	res = PQexecParams(sqlconn,
			"SELECT * FROM users WHERE user_id = $1",
			1, NULL, params ,NULL, NULL, 1);
	if (PQresultStatus(res) != PGRES_TUPLES_OK) {
		PQclear(res);
		return 1;
	}
	if (PQntuples(res) == 1) {
		PQclear(res);
		return 0;
	}
	PQclear(res);
	
	/* no result; means we will insert a new tuple */
	res = PQexecParams(sqlconn,
			"INSERT INTO users VALUES ($1, NULL, NULL)",
			1, NULL, params, NULL, NULL, 1);
	if (PQresultStatus(res) != PGRES_COMMAND_OK) {
		PQclear(res);
		return 1;
	}
	PQclear(res);

	return 0;
}


int db_save_identity(char *user_id, char *identity_dump)
{
	PGresult *res;
	const char *params[2];
	int rc;

	rc = ensure_user(user_id);
	if (rc) {
		return 1;
	}
	
	params[0] = identity_dump;
	params[1] = user_id;
	res = PQexecParams(sqlconn,
			"UPDATE users SET identity_dump = $1 WHERE user_id = $2",
			2, NULL, params, NULL, NULL, 1);
	if (PQresultStatus(res) != PGRES_COMMAND_OK) {
		PQclear(res);
		return 1;
	}

	PQclear(res);
	return 0;
}

int db_save_session(char *user_id, char *session_dump)
{
	PGresult *res;
	const char *params[2];
	int rc;

	rc = ensure_user(user_id);
	if (rc) {
		return 1;
	}
	
	params[0] = session_dump;
	params[1] = user_id;
	res = PQexecParams(sqlconn,
			"UPDATE users SET session_dump = $1 WHERE user_id = $2",
			2, NULL, params, NULL, NULL, 1);
	if (PQresultStatus(res) != PGRES_COMMAND_OK) {
		PQclear(res);
		return 1;
	}

	PQclear(res);
	return 0;
}

int db_save_name_identifier(char *name_identifier, char *user_id)
{
	PGresult *res;
	const char *params[2];

	params[0] = name_identifier;
	params[1] = user_id;

	res = PQexecParams(sqlconn,
			"SELECT * FROM nameidentifiers "
			"WHERE name_identifier = $1",
			1, NULL, params ,NULL, NULL, 1);
	if (PQresultStatus(res) != PGRES_TUPLES_OK) {
		PQclear(res);
		return 1;
	}
	if (PQntuples(res) == 1) {
		PQclear(res);
		return 0;
	}

	res = PQexecParams(sqlconn,
			"INSERT INTO nameidentifiers VALUES ($1, $2)",
			2, NULL, params, NULL, NULL, 1);
	if (PQresultStatus(res) != PGRES_COMMAND_OK) {
		PQclear(res);
		return 1;
	}
	PQclear(res);

	return 0;
}

int db_save_artifact(char *artifact, char *user_id, char *provider_id)
{
	PGresult *res;
	const char *params[3];

	params[0] = artifact;
	params[1] = user_id;
	params[2] = provider_id;

	res = PQexecParams(sqlconn,
			"INSERT INTO artifacts VALUES ($1, $2, $3)",
			3, NULL, params, NULL, NULL, 1);
	if (PQresultStatus(res) != PGRES_COMMAND_OK) {
		PQclear(res);
		return 1;
	}
	PQclear(res);

	return 0;
}

int db_get_artifact(char *artifact, char **user_id, char **provider_id)
{
	PGresult *res;
	const char *params[1];

	params[0] = artifact;

	res = PQexecParams(sqlconn,
			"SELECT user_id, provider_id FROM artifacts "
			"WHERE artifact = $1",
			1, NULL, params, NULL, NULL, 1);
	if (PQresultStatus(res) != PGRES_TUPLES_OK) {
		PQclear(res);
		return 1;
	}

	if (PQntuples(res) == 0) {
		fprintf(stderr, "res: 0 tuples\n");
		PQclear(res);
		return 2;
	}

	*user_id = strdup(PQgetvalue(res, 0, 0));
	*provider_id = strdup(PQgetvalue(res, 0, 1));
	PQclear(res);
	return 0;
}

int db_remove_artifact(char *artifact)
{
	PGresult *res;
	const char *params[1];

	params[0] = artifact;

	res = PQexecParams(sqlconn,
			"DELETE FROM artifacts WHERE artifact = $1",
			1, NULL, params, NULL, NULL, 1);
	if (PQresultStatus(res) != PGRES_COMMAND_OK) {
		PQclear(res);
		return 1;
	}
	return 0;
}

int db_get_user_id(char *name_identifier, char **user_id)
{
	PGresult *res;
	const char *params[1];

	params[0] = name_identifier;

	res = PQexecParams(sqlconn,
			"SELECT user_id FROM nameidentifiers "
			"WHERE name_identifier = $1",
			1, NULL, params, NULL, NULL, 1);
	if (PQresultStatus(res) != PGRES_TUPLES_OK) {
		PQclear(res);
		return 1;
	}

	if (PQntuples(res) == 0) {
		PQclear(res);
		return 1;
	}

	*user_id = strdup(PQgetvalue(res, 0, 0));
	PQclear(res);
	return 0;
}



void db_finish()
{
	if (sqlconn) {
		PQfinish(sqlconn);
		sqlconn = NULL;
	}
}

#endif /* USE_POSTGRESQL */
