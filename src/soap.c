/*
 *  idpc - IDP as a C CGI program
 *  Copyright (C) 2004-2005 Entr'ouvert
 * 
 *  Authors: See AUTHORS file in top-level directory.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "idpc.h"
#include <neon/ne_request.h>
#include <neon/ne_socket.h>

int ssl_verify_fn(void *userdata, int failures, const ne_ssl_certificate *cert);

/* takes response body chunks and appends them to a buffer. */
static void collector(void *ud, const char *data, size_t len)
{
	ne_buffer *buf = ud;
	ne_buffer_append(buf, data, len);
}

int ssl_verify_fn(void *userdata, int failures, const ne_ssl_certificate *cert)
{
	/* we consider every server certificate as ok;
	 * easy debugging, lousy security
	 */
	return 0;
}


char* soap_request(char *url, char *body, char *content_type)
{
	ne_session *sess;
	ne_uri parsed_uri;
	ne_request *req;
	ne_buffer *buf = ne_buffer_create();
	char *answer;
	const ne_status *status;
	int rc;

	rc = ne_uri_parse(url, &parsed_uri);
	if (rc) {
		fprintf(stderr, "ne_uri_parse failed\n");
		return NULL;
	}
	if (strcmp(parsed_uri.scheme, "https") == 0 && ! parsed_uri.port) {
		/* some neon version are buggy and don't set a default port */
		parsed_uri.port = 443;
	}

	ne_sock_init();
	sess = ne_session_create(parsed_uri.scheme, parsed_uri.host,
			parsed_uri.port);
	ne_ssl_set_verify(sess, ssl_verify_fn, NULL);

	req = ne_request_create(sess, "POST", parsed_uri.path);
	ne_add_response_body_reader(req, ne_accept_2xx, collector, buf);
	ne_add_request_header(req, "Content-type",
			content_type ? content_type : "text/xml");
	ne_set_request_body_buffer(req, body, strlen(body));

	rc = ne_request_dispatch(req);
	if (rc) {
		fprintf(stderr, "ne_request_dispatch failed, %d\n", rc);
		fprintf(stderr, "Error was: %s\n", ne_get_error(sess));
		ne_buffer_destroy(buf);
		ne_request_destroy(req);
		ne_session_destroy(sess);
		return NULL;
	}

	status = ne_get_status(req);
	if (status->code == 204) { /* OK; no content */
		ne_buffer_destroy(buf);
		ne_request_destroy(req);
		ne_session_destroy(sess);
		return NULL;
	}

	if (status->code != 200) {
		fprintf(stderr, "status->code = %d\n", status->code);
		ne_buffer_destroy(buf);
		ne_request_destroy(req);
		ne_session_destroy(sess);
		return NULL;
	}

	answer = strdup(buf->data);
	ne_buffer_destroy(buf);
	ne_request_destroy(req);
	ne_session_destroy(sess);

	return answer;
}

char* soap_error(char *msg)
{
	char *template = 
		"<?xml version=\"1.0\"?>\n"
		"<soap:Envelope\n"
		"  xmlns:soap=\"http://www.w3.org/2001/12/soap-envelope\"\n"
		"  soap:encodingStyle=\"http://www.w3.org/2001/12/soap-encoding\">\n"
		"  <soap:Body>\n"
		"    <soap:Fault>\n"
		"      <soap:faultCode>Server</soap:faultCode>\n"
		"      <soap:faultString>%s</soap:faultString>\n"
		"    </soap:Fault>\n"
		"  </soap:Body>\n"
		"</soap:Envelope>\n";
	char *answer;

	answer = malloc(strlen(template) + strlen(msg));
	sprintf(answer, template, msg);
	return answer;
}

