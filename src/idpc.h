/*
 *  idpc - IDP as a C CGI program
 *  Copyright (C) 2004-2005 Entr'ouvert
 * 
 *  Authors: See AUTHORS file in top-level directory.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <time.h>

#include <openssl/x509.h>
#include <openssl/ocsp.h>
#include <openssl/ssl.h>
#include <lasso/lasso.h>
#include <lasso/errors.h> /* lasso bug; shouldn't have to include this */

int error_page(char *msg);
int handle_args(int argc, char *argv[]);
char** urlencoded_to_strings(char *str);
int set_profile_from_dumps(LassoProfile *profile,
			   char *identity_dump, char *session_dump);
int set_profile_auto(LassoProfile *profile);
int save_profile_dumps(LassoProfile *profile);
char* strtime(time_t tim);

int init_config();
char* get_config_string(char *xpath);
LassoServer* get_config_server();

int db_init();
int db_get_dumps(char *user_id, char **identity_dump, char **session_dump);
int db_save_identity(char *user_id, char *identity_dump);
int db_save_session(char *user_id, char *session_dump);
int db_save_name_identifier(char *name_identifier, char *user_id);
int db_save_artifact(char *artifact, char *user_id, char *provider_id);
int db_get_artifact(char *artifact, char **user_id, char **provider_id);
int db_remove_artifact(char *artifact);
int db_get_user_id(char *name_identifier, char **user_id);
void db_finish();

char* soap_request(char *url, char *body, char *content_type);
char* soap_error(char *msg);

struct authentication {
	char *name;
	char* (*auth_function) ();
	const char *lasso_name;
};

struct authentication* get_authentication(char *auth_method);
