/*
 *  idpc - IDP as a C CGI program
 *  Copyright (C) 2004-2005 Entr'ouvert
 * 
 *  Authors: See AUTHORS file in top-level directory.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "idpc.h"
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

static xmlXPathContextPtr xpathCtx;

int init_config()
{
	xmlDocPtr config;
	int rc;

	config = xmlParseFile(SYSCONFDIR "config.xml");
	if (config == NULL) {
		/* parsing failed */
		return 1;
	}

	xpathCtx = xmlXPathNewContext(config);
	if (config == NULL) {
		/* failed to initialize xpath context */
		return 2;
	}

	rc = xmlXPathRegisterNs(xpathCtx,
			"idpc", "http://www.entrouvert.org/namespaces/idpc");

	return 0;
}

/**
 * get_config_string:
 * @xpath: xpath expression to configuration node
 *
 * Gets a value defined in the configuration file.
 *
 * Return value: the value; or NULL if not found.  The string is internally
 *     allocated.  It must not be freed or modified.
 **/
char* get_config_string(char *xpath)
{
	xmlXPathObjectPtr xpathObj;
	char *content;

	xpathObj = xmlXPathEval(xpath, xpathCtx);
	if (xpathObj == NULL || xpathObj->nodesetval == NULL || \
			xpathObj->nodesetval->nodeNr == 0) {
		if (xpathObj) xmlXPathFreeObject(xpathObj);
		return NULL;
	}

	content = xpathObj->nodesetval->nodeTab[0]->children->content;
	xmlXPathFreeObject(xpathObj);
	return content;
}

LassoServer* get_config_server()
{
	LassoServer *server;
	int i = 0;
	char cfg_metadata[60], cfg_publickey[60], cfg_cacertificate[60];
	int rc;

	server = lasso_server_new(
			get_config_string("//idpc:metadataFilePath"),
			get_config_string("//idpc:idpPrivateKey"),
			NULL,
			get_config_string("//idpc:idpCertificate"));
	if (server == NULL) {
		fprintf(stderr, "failure in server_new\n");
		return NULL;
	}

	while (++i) {
		snprintf(cfg_metadata, 60,
			"//idpc:serviceProvider[%d]/idpc:metadataFilePath", i);
		snprintf(cfg_publickey, 60,
			"//idpc:serviceProvider[%d]/idpc:spPublicKey", i);
		snprintf(cfg_cacertificate, 60,
			"//idpc:serviceProvider[%d]/idpc:spCaCertificate", i);
		if (get_config_string(cfg_metadata) == NULL) {
			/* no more service provider */
			break;
		}
		rc = lasso_server_add_provider(server,
				LASSO_PROVIDER_ROLE_SP,
				get_config_string(cfg_metadata),
				get_config_string(cfg_publickey),
				get_config_string(cfg_cacertificate));
		if (rc) {
			fprintf(stderr, "failure in add_provider\n");
			return NULL;
		}
	}

	return server;
}

