/*
 *  idpc - IDP as a C CGI program
 *  Copyright (C) 2004-2005 Entr'ouvert
 * 
 *  Authors: See AUTHORS file in top-level directory.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "idpc.h"

#ifdef USE_MYSQL

#include <mysql/mysql.h>

static MYSQL *sqlconn;

int db_init()
{
	/* TODO: get parameters from config */
	sqlconn = mysql_init(NULL);
	if ( ! mysql_real_connect(sqlconn,
			get_config_string("//idpc:dbhost"),
			get_config_string("//idpc:dblogin"),
			get_config_string("//idpc:dbpassword"),
			get_config_string("//idpc:dbname"),
			0, /* port, need get_config_int */
			NULL, 0) ) {
		return 1;
	}
	return 0;
}

int db_get_dumps(char *user_id, char **identity_dump, char **session_dump)
{
	MYSQL_RES *res;
	MYSQL_ROW row;
	char query[500];
	int rc;

	snprintf(query, 500,
		"SELECT identity_dump, session_dump FROM users WHERE user_id = '%s'",
		user_id);
	if (mysql_query(sqlconn, query)) {
		return 1;
	}
	res = mysql_store_result(sqlconn);
	if (mysql_num_rows(res) == 0) {
		mysql_free_result(res);
		*identity_dump = NULL;
		*session_dump = NULL;
		return 0;
	}
	row = mysql_fetch_row(res);
	*identity_dump = strdup(row[0]);
	*session_dump = strdup(row[1]);
	mysql_free_result(res);
	return 0;
}

static int ensure_user(char *user_id)
{
	MYSQL_RES *res;
	MYSQL_ROW row;
	char query[500];
	int rc;

	snprintf(query, 500,
		"SELECT * FROM users WHERE user_id = '%s'",
		user_id);
	if (mysql_query(sqlconn, query)) {
		return 1;
	}
	res = mysql_store_result(sqlconn);
	if (mysql_num_rows(res) == 1) {
		mysql_free_result(res);
		return 0;
	}
	mysql_free_result(res);

	snprintf(query, 500,
		"INSERT INTO users VALUES ('%s', NULL, NULL)",
		user_id);
	if (mysql_query(sqlconn, query)) {
		return 1;
	}
	
	return 0;
}

int db_save_identity(char *user_id, char *identity_dump)
{
	int rc;
	char *query;

	rc = ensure_user(user_id);
	if (rc) {
		return 1;
	}

	query = malloc(500 + strlen(identity_dump));
	snprintf(query, 500 + strlen(identity_dump),
		"UPDATE users SET identity_dump = '%s' WHERE user_id = '%s'",
		identity_dump,
		user_id);
	if (mysql_query(sqlconn, query)) {
		return 1;
	}

	free(query);
	return 0;
}

int db_save_session(char *user_id, char *session_dump)
{
	int rc;
	char *query;

	rc = ensure_user(user_id);
	if (rc) {
		return 1;
	}

	query = malloc(500 + strlen(session_dump));
	snprintf(query, 500 + strlen(session_dump),
		"UPDATE users SET session_dump = '%s' WHERE user_id = '%s'",
		session_dump,
		user_id);
	if (mysql_query(sqlconn, query)) {
		return 1;
	}

	free(query);
	return 0;
}



void db_finish()
{
	if (sqlconn) {
		mysql_close(sqlconn);
		sqlconn = NULL;
	}
}

#endif /* USE_MYSQL */

