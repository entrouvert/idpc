::

  CREATE USER idpc PASSWORD 'pass';

  CREATE TABLE nameidentifiers (
  	name_identifier	varchar(100) primary key,
  	user_id		varchar(100)
  );
  
  CREATE TABLE users (
  	user_id		varchar(100) primary key,
  	identity_dump	text,
  	session_dump	text
  );
  
  CREATE TABLE artifacts (
        artifact        varchar(100) primary key,
	user_id         varchar(100),
	provider_id     text
  );
  
  GRANT DELETE, INSERT, SELECT, UPDATE ON nameidentifiers TO idpc;
  GRANT DELETE, INSERT, SELECT, UPDATE ON users TO idpc;
  GRANT DELETE, INSERT, SELECT, UPDATE ON artifacts TO idpc;
